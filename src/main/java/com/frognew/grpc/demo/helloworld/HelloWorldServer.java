package com.frognew.grpc.demo.helloworld;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

public class HelloWorldServer {

	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldServer.class);

	private Server server;
	private Server server2;

	private void start() throws IOException {
		/* The port on which the server should run */
		int port = 50051;
		/* register Greeter, Service2 service for port 50051 server */
		server = ServerBuilder.forPort(port).addService(new GreeterImpl()).addService(new Service2Impl()).build()
				.start();
		LOGGER.info("Server started, listening on " + port);
		/* can run multi server */
		int port2 = 50052;
		/* register Service2 service for port 50052 server */
		server2 = ServerBuilder.forPort(port2).addService(new Service2Impl()).build().start();
		LOGGER.info("Server2 started, listening on " + port2);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// Use stderr here since the logger may have been reset by its JVM shutdown
				// hook.
				System.err.println("*** shutting down gRPC server since JVM is shutting down");
				HelloWorldServer.this.stop();
				System.err.println("*** server shut down");
			}
		});
	}

	private void stop() {
		if (server != null) {
			server.shutdown();
		}
		if (server2 != null) {
			server2.shutdown();
		}
	}

	/**
	 * Await termination on the main thread since the grpc library uses daemon
	 * threads.
	 */
	private void blockUntilShutdown() throws InterruptedException {
		if (server != null) {
			server.awaitTermination();
		}
		if (server2 != null) {
			server2.awaitTermination();
		}
	}

	/**
	 * Main launches the server from the command line.
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		final HelloWorldServer server = new HelloWorldServer();
		server.start();
		server.blockUntilShutdown();
	}

	/**
	 * implement Greeter Service
	 * 
	 * @author eason
	 *
	 */
	static class GreeterImpl extends GreeterGrpc.GreeterImplBase {

		@Override
		public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
			HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName()).build();
			responseObserver.onNext(reply);
			responseObserver.onCompleted();
		}

		@Override
		public void test(TestRequest req, StreamObserver<TestReply> testObserver) {
			TestReply reply = TestReply.newBuilder().setResponse("id is: " + req.getId() + ", pwd is: " + req.getPwd())
					.build();
			testObserver.onNext(reply);
			testObserver.onCompleted();
		}
	}

	/**
	 * implement Service2 Service
	 * 
	 * @author eason
	 *
	 */
	static class Service2Impl extends Service2Grpc.Service2ImplBase {
		@Override
		public void testService2(TestService2Request req, StreamObserver<TestService2Reply> testService2Observer) {
			TestService2Reply reply = TestService2Reply.newBuilder()
					.setResponse("name is: " + req.getName() + ", age is: " + req.getAge()).build();
			testService2Observer.onNext(reply);
			testService2Observer.onCompleted();
		}
	}
}
